import React from 'react'
import { View,StyleSheet, Button, TouchableOpacity} from 'react-native'

const ConnexionInscription = ({Connexion,Inscription}) => {
  return (
      <View>
      <TouchableOpacity style ={styles.container}>
    <Button title="Connexion" onPress={Connexion}/> 
    </TouchableOpacity>
       <TouchableOpacity style ={styles.container}>
       <Button title="Inscription" onPress={Inscription}/> 
       </TouchableOpacity>
       </View>
  )
}
const styles =StyleSheet.create({
    container:{
        backgroundColor:'white',
        width:'100%',
        borderColor:'#e8e8e8',
        borderWidth:1,
        borderRadius:6,
        paddingHorizontal:10,
        marginVertical:10,
    },
    input:{},
})
export default ConnexionInscription