import React from 'react'
import { View, Text,TextInput,StyleSheet,Image, Button} from 'react-native'

const InscriptionScreen =() =>{
  return (
<View>
    <Image source={require('../../assets/images/Logo_1.png')} style={styles.logo} resizeMode="contain" />

    <Text style={styles.Text}>Nom</Text>
<TextInput style={styles.Input}
placeholder="Sagnon"
placeholderTextColor="black"
/>
<Text style={styles.Text}>Prénom</Text>
<TextInput style={styles.Input}
placeholder="Alphonse"
placeholderTextColor="black"
/>
<Text style={styles.Text}>Numéro de Téléphone</Text>
<TextInput style={styles.Input}
placeholder="+226 98 63 ... ..."
placeholderTextColor="black"
/>
<Button style={styles.Ok} title="OK" />
</View>  )
}

const styles = StyleSheet.create({
    Text:{
        fontSize:20,
    },
Input:{
    backgroundColor:'white',
    height: 60,
    margin: 16,
    borderWidth: 1,
    padding: 20,
    borderRadius: 6,
    borderColor: '#e8e8e8'},

    logo:{
        height:100,
        alignItems:'center',
        alignContent:'center',
        marginBottom:20,
        marginTop:40,
    },
    Ok:{
        backgroundColor:'#00157D',
        height: 60,
    margin: 16,
    borderWidth: 1,
    padding: 20,
    borderRadius: 6,
    }


})

export default InscriptionScreen