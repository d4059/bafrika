import { View, Text,StyleSheet,Image} from 'react-native'
import React from 'react'

const Logo = () => {
  return (
    <View>
        <Image source={require('../../assets/images/Logo_1.png')} style={styles.logo} resizeMode="contain" />
      <Text style={styles.logo}>Bafrika</Text>
    </View>
  )
}

const styles = StyleSheet.create({
    baseText: {
        fontFamily: "Herculanum",
      },
    logo: {
        fontFamily: "Herculanum",
        fontSize: 40,
        flex: 1,
    }
    
})
;
export default Logo