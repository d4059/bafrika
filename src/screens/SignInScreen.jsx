import React from "react";
import { View, Image, StyleSheet } from "react-native";
import ConnexionInscription from "../ConnexionIncription/ConnexionInscription";


const SignInScreen = () => {
  return (
    <View style={styles.root}>
              <Image source={require('../../assets/images/Logo_1.png')} style={styles.logo} resizeMode="contain" />
    <ConnexionInscription/>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    padding:10
  },
  logo: {
      height:100

  },
});
export default SignInScreen;
