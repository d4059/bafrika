import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import InscriptionScreen from "./src/screens/InscriptionScreen";
import Logo from "./src/screens/logo";
import SignInScreen from "./src/screens/SignInScreen";
export default function App() {
  return (
    <View style={styles.container}>
      <InscriptionScreen />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "grey",
  },
});
